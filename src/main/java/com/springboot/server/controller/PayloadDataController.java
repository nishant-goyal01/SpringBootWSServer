package com.springboot.server.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.springboot.server.payload.PayloadData;

@RestController
public class PayloadDataController {




	@RequestMapping("/jsonResp/{seqno}")
	@ResponseBody
	public String getPayloadData(@PathVariable String seqno){

		PayloadData payloadData = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			URL url = this.getClass().getClassLoader().getResource("cards.json");
			String uri = url.getFile(); 
			BufferedReader br = new BufferedReader(new InputStreamReader( new FileInputStream(new File(url.getFile()))));
			String jsonData = br.readLine();
			Thread.sleep(1000);
			//payloadData = mapper.readValue(new File(url.getFile()), PayloadData.class);

			//return mapper.writeValueAsString(user);
			return jsonData;//payloadData.toString();//new ResponseEntity<PayloadData>(payloadData, HttpStatus.OK);//
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}


}
