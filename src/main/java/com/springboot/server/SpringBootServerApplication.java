package com.springboot.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.context.annotation.Profile;

@SpringBootApplication
//@Profile("dev")
public class SpringBootServerApplication {


	public static void main(String[] args) {
		SpringApplication.run(SpringBootServerApplication.class, args);

	}
}
