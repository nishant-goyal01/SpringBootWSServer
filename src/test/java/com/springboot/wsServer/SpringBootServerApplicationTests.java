package com.springboot.wsServer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.springboot.server.SpringBootServerApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SpringBootServerApplication.class)
@WebAppConfiguration
public class SpringBootServerApplicationTests {

	@Test
	public void contextLoads() {
	}

}
